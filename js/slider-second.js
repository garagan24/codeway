$('.wrapper1').each(function () {
  var $slider1 = $(this);
  var numberOfslides = $slider1.find('.panel1').length;

  $slider1.find('.panel1:eq(0)').addClass('_active1');
  $slider1.find('.nav-dot1:eq(0)').addClass('active1');

  var $active1slide = $slider1.find('.panel1._active1');
  var $nextBtn = $slider1.find('.next-btn1');
  var $prevBtn = $slider1.find('.prev-btn1');

  $('.nav-dot1').on('click', function () {
    var slideToGo = $(this).data('slide');
    goToslide(slideToGo);
  });


    $slider1.on('slide.changed', function () {
      // console.log('slide changed !');
      $('.nav-dot1').removeClass('active1');
      var $active1Dot = $('.nav-dot1[data-slide="' + $('.panel1._active1').data('slide') + '"]');
      $active1Dot.addClass('active1');
    });




  $nextBtn.on('click', function (event) {
    nextSlide_1();
  });

  $prevBtn.on('click', function (event) {
    prevSlide_1();
  });

  function nextSlide_1() {
    $active1slide = $slider1.find('.panel1._active1');
    var $nextSlide_1 = $active1slide.next('.panel1');
    $active1slide.removeClass('_active1');
    $nextSlide_1.addClass('_active1');

    // $active1slide = $nextSlide_1;

    var slideIndex = $slider1.find('.panel1._active1').index('.panel1');


        if (slideIndex >= numberOfslides || slideIndex <= -1) {
            firstslide();
            $slider1.trigger('slide.changed');

        } else {
            $slider1.trigger('slide.changed');
        }



  }

  function prevSlide_1() {
    $active1slide = $slider1.find('.panel1._active1');

    var $prevSlide_1 = $active1slide.prev('.panel1');

    $active1slide.removeClass('_active1');
    $prevSlide_1.addClass('_active1');

    var slideIndex = $slider1.find('.panel1._active1').index();


    if (typeof $prevSlide_1 === 'undefined' || $prevSlide_1 === null || $prevSlide_1.length == -1 || slideIndex <= -1) {
        lastslide();
        $slider1.trigger('slide.changed');
    } else {
        $slider1.trigger('slide.changed');
    }



  }

  function firstslide() {
    $('.panel1._active1').removeClass('_active1');
    $slider1.find('.panel1:eq(0)').addClass('_active1');
    $active1slide = $slider1.find('.panel1:eq(0)');

  }

  function lastslide() {

    $('.panel1._active1').removeClass('_active1');
    $slider1.find('.panel1').eq(numberOfslides - 1).addClass('_active1');

  }

  function goToslide(slideToGo) {
    $('.panel1._active1').removeClass('_active1');
    $slider1.find('.panel1').eq(slideToGo - 1).addClass('_active1');
    $active1slide = $slider1.find('.panel1').eq(slideToGo - 1).addClass('_active1');
    $slider1.trigger('slide.changed');
  }

  var slider2 = document.querySelector('#pswrap');

  var indicator = new WheelIndicator({
    elem: slider2,
    callback: function(e){
      if (e.direction === 'down') {
        prevSlide_1()
      } else {
        nextSlide_1()
      }
    }
  });
  //The method call
  indicator.getOption('preventMouse'); // true

  var initialPoint;
  var finalPoint;
  var tarObj1 = document.querySelector('.wrapper1')
  tarObj1.addEventListener('touchstart', function(event) {
    initialPoint=event.changedTouches[0];
  }, false);
  tarObj1.addEventListener('touchend', function(event) {
    finalPoint=event.changedTouches[0];
    var xAbs = Math.abs(initialPoint.pageX - finalPoint.pageX);
    var yAbs = Math.abs(initialPoint.pageY - finalPoint.pageY);
    if (xAbs > 20 || yAbs > 20 && tarObj) {
      if (xAbs > yAbs) {
        if (finalPoint.pageX < initialPoint.pageX) {
          prevSlide_1()
        }
        else{
          nextSlide_1()
        }
      }
    }
  }, false);

});
