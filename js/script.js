$('.wrapper').each(function () {
  var $slider = $(this);
  var numberOfSlides = $slider.find('.panel').length;

  // let arr = ["zapupa", "sula", "blyasd"];
  // alert();

  $slider.find('.panel:eq(0)').addClass('_active');
  $slider.find('.nav-dot:eq(0)').addClass('active');

  var $activeSlide = $slider.find('.panel._active');
  var $nextBtn = $slider.find('.next-btn');
  var $prevBtn = $slider.find('.prev-btn');

  $('.nav-dot').on('click', function () {
    var slideToGo = $(this).data('slide');
    goToSlide(slideToGo);
  });

  $slider.on('slide.changed', function () {
    setTimeout(function (){
      $('.nav-dot').removeClass('active');
      var $activeDot = $('.nav-dot[data-slide="' + $('.panel._active').data('slide') + '"]');
      $activeDot.addClass('active');
    }, 2000)

  });

  $nextBtn.on('click', function (event) {
    nextSlide();
  });

  $prevBtn.on('click', function (event) {
    prevSlide();
  });

  function nextSlide() {
    $activeSlide = $slider.find('.panel._active');
    var $nextSlide = $activeSlide.next('.panel');
    $activeSlide.removeClass('_active');
    $nextSlide.addClass('_active');

    //$activeSlide = $nextSlide;

    var slideIndex = $slider.find('.panel._active').index('.panel');
    setTimeout(function (){
      if (slideIndex >= numberOfSlides || slideIndex <= -1) {
        firstSlide();
        $slider.trigger('slide.changed');

      } else {
        $slider.trigger('slide.changed');
      }
    }, 2000)

  }

  function prevSlide() {
    $activeSlide = $slider.find('.panel._active');

    var $prevSlide = $activeSlide.prev('.panel');

    $activeSlide.removeClass('_active');
    $prevSlide.addClass('_active');

    var slideIndex = $slider.find('.panel._active').index();

    setTimeout(function (){
      if (typeof $prevSlide === 'undefined' || $prevSlide === null || $prevSlide.length == -1 || slideIndex <= -1) {
        lastSlide();
        $slider.trigger('slide.changed');
      } else {
        $slider.trigger('slide.changed');
      }
    }, 2000)

  }

  function firstSlide() {
    $('.panel._active').removeClass('_active');
    $slider.find('.panel:eq(0)').addClass('_active');
    $activeSlide = $slider.find('.panel:eq(0)');
    // $('body').addClass('stop')
  }

  function lastSlide() {
    $('.panel._active').removeClass('_active');
    $slider.find('.panel').eq(numberOfSlides - 1).addClass('_active');
  }

  // if ('wg-slider') {
  //   $('body').addClass('stop')
  // } else {
  //   $('body').removeClass('stop')

  // }

  function goToSlide(slideToGo) {
    $('.panel._active').removeClass('_active');
    $slider.find('.panel').eq(slideToGo - 1).addClass('_active');
    $activeSlide = $slider.find('.panel').eq(slideToGo - 1).addClass('_active');
    $slider.trigger('slide.changed');
  }

  // var slider = $(".wg-slider-img");
  // slider.on('wheel', (function (e) {
  //   e.preventDefault();
  //
  //   if (e.originalEvent.deltaY > 0) {
  //     nextSlide()
  //   } else {
  //     prevSlide()
  //   }
  // }));

  var sliderr = document.querySelector('.psevdo-wrapper');

  var indicator = new WheelIndicator({
    elem: sliderr,
    callback: function(e){
      if (e.direction === 'down' || e.direction.getDeltaY) {
        prevslide()
      } else {
        nextslide()
      }
    }
  });
});



const slider = $(".slider-item");
slider
    .slick({
      dots: true
    });

slider.on('wheel', (function(e) {
  e.preventDefault();

  if (e.originalEvent.deltaY < 0) {
    $(this).slick('slickNext');
  } else {
    $(this).slick('slickPrev');
  }
}));
