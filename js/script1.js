$('.wrapper1').each(function () {
  var $slider1 = $(this);
  var numberOfslides = $slider1.find('.panel1').length;

  $slider1.find('.panel1:eq(0)').addClass('_active1');
  $slider1.find('.nav-dot1:eq(0)').addClass('active1');

  var $active1slide = $slider1.find('.panel1._active1');
  var $nextBtn = $slider1.find('.next-btn1');
  var $prevBtn = $slider1.find('.prev-btn1');

  $('.nav-dot1').on('click', function () {
    var slideToGo = $(this).data('slide');
    goToslide(slideToGo);
  });

    $slider1.on('slide.changed', function () {
      $('.nav-dot1').removeClass('active1');
      var $active1Dot = $('.nav-dot1[data-slide="' + $('.panel1._active1').data('slide') + '"]');
      $active1Dot.addClass('active1');
    });



    $('.nav-dot1').removeClass('active1');
    var $active1Dot = $('.nav-dot1[data-slide="' + $('.panel1._active1').data('slide') + '"]');
    $active1Dot.addClass('active1');

  $nextBtn.on('click', function (event) {
    nextslide();
  });

  $prevBtn.on('click', function (event) {
    prevslide();
  });

  function nextslide() {
    $active1slide = $slider1.find('.panel1._active1');
    var $nextslide = $active1slide.next('.panel1');
    $active1slide.removeClass('_active1');
    $nextslide.addClass('_active1');

    //$active1slide = $nextslide;

    var slideIndex = $slider1.find('.panel1._active1').index('.panel1');
      if (slideIndex >= numberOfslides || slideIndex <= -1) {
        firstslide();
        $slider1.trigger('slide.changed');

      } else {
        $slider1.trigger('slide.changed');
      }

  }

  function prevslide() {
    $active1slide = $slider1.find('.panel1._active1');

    var $prevslide = $active1slide.prev('.panel1');

    $active1slide.removeClass('_active1');
    $prevslide.addClass('_active1');

    var slideIndex = $slider1.find('.panel1._active1').index();

      if (typeof $prevslide === 'undefined' || $prevslide === null || $prevslide.length == -1 || slideIndex <= -1) {
        lastslide();
        $slider1.trigger('slide.changed');
      } else {
        $slider1.trigger('slide.changed');
      }


  }

  function firstslide() {
    $('.panel1._active1').removeClass('_active1');
    $slider1.find('.panel1:eq(0)').addClass('_active1');
    $active1slide = $slider1.find('.panel1:eq(0)');

  }

  function lastslide() {

    $('.panel1._active1').removeClass('_active1');
    $slider1.find('.panel1').eq(numberOfslides - 1).addClass('_active1');

  }

  function goToslide(slideToGo) {
    $('.panel1._active1').removeClass('_active1');
    $slider1.find('.panel1').eq(slideToGo - 1).addClass('_active1');
    $active1slide = $slider1.find('.panel1').eq(slideToGo - 1).addClass('_active1');
    $slider1.trigger('slide.changed');
  }

  var sliderr = document.querySelector('.psevdo-wrapper');

  var indicator = new WheelIndicator({
    elem: sliderr,
    callback: function(e){
      if (e.direction === 'down' || e.direction.getDeltaY) {
        prevslide()
      } else {
        nextslide()
      }
    }
  });
});
