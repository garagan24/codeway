// SmoothScroll({
//   // Время скролла 400 = 0.4 секунды
//   animationTime: 1000,
//   // Размер шага в пикселях
//   stepSize: 500,
//
//   // Дополнительные настройки:
//
//   // Ускорение
//   accelerationDelta: 10,
//   // Максимальное ускорение
//   accelerationMax: 2,
//
//   // Поддержка клавиатуры
//   keyboardSupport: true,
//   // Шаг скролла стрелками на клавиатуре в пикселях
//   arrowScroll: 500,
//
//   // Pulse (less tweakable)
//   // ratio of "tail" to "acceleration"
//   pulseAlgorithm: true,
//   pulseScale: 4,
//   pulseNormalize: 1,
//
//   // Поддержка тачпада
//   touchpadSupport: true,
// })
wow = new WOW(
  {
    boxClass: 'wow',      // default
    animateClass: 'animated', // change this if you are not using animate.css
    offset: 0,          // default
    mobile: true,       // keep it on mobile
    live: true        // track if element updates
  }
)
wow.init();


$(window).scroll(
  {
    previousTop: 0
  },
  function () {
    var currentTop = $(window).scrollTop();
    if (currentTop < this.previousTop) {
      $(".main-menu").show()
    } else {
      $(".main-menu").hide()
    }
    if (currentTop != 0) {
      $(".main-menu").addClass("menu-sticky");
    } else {
      $(".main-menu").removeClass("menu-sticky");
    }
    this.previousTop = currentTop;
  });


