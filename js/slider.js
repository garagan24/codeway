$('.wrapper').each(function () {
  var $slider = $(this);
  var numberOfSlides = $slider.find('.panel').length;

  $slider.find('.panel:eq(0)').addClass('_active');
  $slider.find('.nav-dot:eq(0)').addClass('active');

  var $activeSlide = $slider.find('.panel._active');
  var $nextBtn = $slider.find('.next-btn');
  var $prevBtn = $slider.find('.prev-btn');

  $('.nav-dot').on('click', function () {
    var slideToGo = $(this).data('slide');
    goToSlide(slideToGo);
  });

  $slider.on('slide.changed', function () {
    $('.nav-dot').removeClass('active');
    var $activeDot = $('.nav-dot[data-slide="' + $('.panel._active').data('slide') + '"]');
    $activeDot.addClass('active');
  });

  $nextBtn.on('click', function (event) {
    nextSlide();
  });

  $prevBtn.on('click', function (event) {
    prevSlide();
  });

  function nextSlide() {
    $activeSlide = $slider.find('.panel._active');
    var $nextSlide = $activeSlide.next('.panel');
    $activeSlide.removeClass('_active');
    $nextSlide.addClass('_active');

    //$activeSlide = $nextSlide;

    var slideIndex = $slider.find('.panel._active').index('.panel');


      if (slideIndex >= numberOfSlides || slideIndex <= -1) {
        firstSlide();
        $slider.trigger('slide.changed');

      } else {
        $slider.trigger('slide.changed');
      }

  }

  function prevSlide() {
    $activeSlide = $slider.find('.panel._active');

    var $prevSlide = $activeSlide.prev('.panel');

    $activeSlide.removeClass('_active');
    $prevSlide.addClass('_active');

    var slideIndex = $slider.find('.panel._active').index();

      if (typeof $prevSlide === 'undefined' || $prevSlide === null || $prevSlide.length == -1 || slideIndex <= -1) {
        lastSlide();
        $slider.trigger('slide.changed');
      } else {
        $slider.trigger('slide.changed');
      }



  }

  function firstSlide() {
    $('.panel._active').removeClass('_active');
    $slider.find('.panel:eq(0)').addClass('_active');
    $activeSlide = $slider.find('.panel:eq(0)');
    // $('body').addClass('stop')
  }

  function lastSlide() {
    $('.panel._active').removeClass('_active');
    $slider.find('.panel').eq(numberOfSlides - 1).addClass('_active');
  }

  // if ('wg-slider') {
  //   $('body').addClass('stop')
  // } else {
  //   $('body').removeClass('stop')

  // }

  function goToSlide(slideToGo) {
    $('.panel._active').removeClass('_active');
    $slider.find('.panel').eq(slideToGo - 1).addClass('_active');
    $activeSlide = $slider.find('.panel').eq(slideToGo - 1).addClass('_active');
    $slider.trigger('slide.changed');
  }

  var slider1 = document.querySelector('.psevdo-wrapper');

  var indicator = new WheelIndicator({
    elem: slider1,
    callback: function(e){
      if (e.direction === 'down') {
        prevSlide()
      } else {
        nextSlide()
      }
    }
  });

  //The method call
  indicator.getOption('preventMouse'); // true

  var initialPoint;
  var finalPoint;
  var tarObj = document.querySelector('.wrapper')
  tarObj.addEventListener('touchstart', function(event) {
    initialPoint=event.changedTouches[0];
  }, false);
  tarObj.addEventListener('touchend', function(event) {
    finalPoint=event.changedTouches[0];
    var xAbs = Math.abs(initialPoint.pageX - finalPoint.pageX);
    var yAbs = Math.abs(initialPoint.pageY - finalPoint.pageY);
    if (xAbs > 20 || yAbs > 20 && tarObj) {
      if (xAbs > yAbs) {
        if (finalPoint.pageX < initialPoint.pageX) {
          prevSlide()
        }
        else{
          nextSlide()
        }
      }
    }
  }, false);
});
