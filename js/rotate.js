let rotatebox = document.querySelector('.rotate');
gsap.to(".rotate", {
  rotation: 360, duration: 20, repeat: -1, ease:
    Power0.easeNone
});

rotatebox.onclick = function () {
  gsap.fromTo(".rotate", { rotationY: 0, x: 0, y: 0 }, { rotationY: 180, rotation: 360, x: -30, y: -30, duration: 1 })
    .then(res => { gsap.fromTo(".rotate", { x: -30, y: -30 }, { x: 0, y: 0, duration: 5 }) });
}

let rotatebox1 = document.querySelector('.rotate1');
gsap.to(".rotate1", {
  rotation: 360, duration: 20, repeat: -1, ease:
    Power0.easeNone
});

rotatebox1.onclick = function () {
  gsap.fromTo(".rotate1", { rotationX: 0 }, { rotationX: 360, duration: 1 });
}

let rotatebox2 = document.querySelector('.rotate2');
gsap.to(".rotate2", {
  rotation: 360, duration: 20, repeat: -1, ease:
    Power0.easeNone
});

rotatebox2.onclick = function () {
  gsap.fromTo(".rotate2", { rotationX: 0 }, { rotationX: 360, duration: 1 });
}

let rotatebox3 = document.querySelector('.rotate3');
gsap.to(".rotate3", {
  rotation: 360, duration: 20, repeat: -1, ease:
    Power0.easeNone
});

rotatebox3.onclick = function () {
  gsap.fromTo(".rotate3", { rotationX: 0 }, { rotationX: -360, duration: 1 });
}

let rotatebox4 = document.querySelector('.rotate4');
gsap.to(".rotate4", {
  rotation: -360, duration: 20, repeat: -1, ease:
    Power0.easeNone
});

rotatebox4.onclick = function () {
  gsap.fromTo(".rotate4", { rotationX: 0 }, { rotationX: 360, duration: 1 });
}

let rotatebox5 = document.querySelector('.rotate5');
gsap.to(".rotate5", {
  rotation: -360, duration: 20, repeat: -1, ease:
    Power0.easeNone
});

rotatebox5.onclick = function () {
  gsap.fromTo(".rotate5", { rotationX: 0 }, { rotationX: 360, duration: 1 });
}