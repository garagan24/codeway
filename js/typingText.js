typed = new Typed('#typing', {
  strings: ['Apps', 'Games'],
  typeSpeed: 100,
  backSpeed: 100,
  backDelay: 1000,
  startDelay: 1000,
  loop: true,
  cursorChar: '/',
});